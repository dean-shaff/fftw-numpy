import sys

import numpy as np
import matplotlib.pyplot as plt


def power(arr):
    return np.abs(arr)**2


def db(arr):
    return 10*np.log10(arr)


def main():

    input_file_path, fftw_r2c_file_path = sys.argv[1:3]

    input_data = np.fromfile(input_file_path, dtype=np.float64)
    fftw_r2c_data = np.fromfile(fftw_r2c_file_path, dtype=np.complex128)
    np_r2c_data = np.fft.rfft(input_data)

    fig, axes = plt.subplots(3, 1, figsize=(15, 10))
    for ax in axes:
        ax.grid(True)

    axes[0].plot(input_data)
    axes[1].plot(db(power(fftw_r2c_data)))
    axes[2].plot(db(power(np_r2c_data)))

    allclose = np.allclose(fftw_r2c_data, np_r2c_data)
    print(f"allclose={allclose}")

    plt.show()



if __name__ == "__main__":
    main()
