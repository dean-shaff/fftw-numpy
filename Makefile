CC=g++

all: main

main: main.cpp
	$(CC) $^ -o $@ -lfftw3

.PHONY: clean
clean:
	rm -f main
