#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <complex>

#include "cxxopts.hpp"
#include "fftw3.h"


template<typename T>
struct fftw2std;

template<>
struct fftw2std<std::complex<double>>
{
  using type = fftw_complex;
};


template<typename T>
void load_binary_data (const std::string& file_path, std::vector<T>& vec)
{
  std::streampos size;

  std::ifstream file (file_path, std::ios::in|std::ios::binary|std::ios::ate);
  if (file.is_open())
  {
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<char> file_bytes(size);
    file.read(&file_bytes[0], size);
    file.close();

    unsigned T_size = (size / sizeof(T));

    const T* data = reinterpret_cast<const T*>(file_bytes.data());
    vec.assign(data, data + T_size);
  }
}

template<typename T>
void dump_binary_data (const std::string& file_path, const std::vector<T>& vec)
{
  auto file = std::fstream(file_path, std::ios::out | std::ios::binary);
  file.write((char*)vec.data(), sizeof(T)*vec.size());
  file.close();
}


void perform_rfft(
  std::vector<double>& signal,
  std::vector<std::complex<double>>& fft
)
{

  int fft_size = static_cast<int>(signal.size());

  fftw_complex* fft_ptr = reinterpret_cast<fftw_complex*>(fft.data());

  fftw_plan plan = fftw_plan_dft_r2c_1d(fft_size, signal.data(), fft_ptr, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);
}



int main (int argc, char** argv) {
  cxxopts::Options options("main", "Perform FFTW r2c on input binary data");
  options.add_options()
    ("i,input-file-path", "Input file path", cxxopts::value<std::string>())
    ("h,help", "Print usage")
  ;

  auto result = options.parse(argc, argv);
  if (result.count("help"))
  {
    std::cout << options.help() << std::endl;
    exit(0);
  }

  std::string input_file_path = result["input-file-path"].as<std::string>();
  // std::cerr << "input_file_path=" << input_file_path << std::endl;
  std::vector<double> input_data;

  load_binary_data(input_file_path, input_data);
  std::cerr << "input data size " << input_data.size() << std::endl;

  unsigned output_size = (input_data.size() / 2) + 1;
  std::vector<std::complex<double>> fft(output_size);

  perform_rfft(input_data, fft);

  dump_binary_data("fft.dat", fft);

}
