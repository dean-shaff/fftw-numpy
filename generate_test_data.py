import numpy as np
import matplotlib.pyplot as plt


def main():
    sampling_rate = 44100 # Hz
    freq = 1000 # Hz

    t0 = np.linspace(0.0, 1.0, sampling_rate)
    signal = np.sin(2*np.pi*freq*t0)

    file_path = f"sin.{freq}.{sampling_rate}.dat"

    with open(file_path, "wb") as fd:
        signal.tofile(fd)

    fig, ax = plt.subplots(1, 1, figsize=(10, 10))

    ax.plot(t0, signal)
    ax.grid(True)
    plt.show()



if __name__ == "__main__":
    main()
